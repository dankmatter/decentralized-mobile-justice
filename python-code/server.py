from time import sleep
import serverLib
import sys
import os


if len(sys.argv) < 3:
    print('Incorrect command invocation')
    print('Proper Usage:\npython3.5 server.py <file-name-path> <file-uploader-path>')
    print('Where <file-name-path> is the path to the file to upload\n<file-uploader-path> is a link to the file containing the uploader')

workingDir = '/root'
contractAbiFile = 'contract_abi.json'
workingDirFiles = os.listdir(workingDir)
dataStoreContractAddress = '0x9Cd9eaE8A59Dac42267522CD73EC9A7B0D79f20d'

if contractAbiFile not in workingDirFiles:
    print('Contract ABI is missing, please create it')
    exit()


web3ctl = serverLib.web3(contractAbiFile, dataStoreContractAddress)
ipfsctl = server.Ipfs('127.0.0.1', 5001)



file_name = sys.argv[1]
file_uploader = sys.argv[2]
ipfsHash = ipfsctl.uploadFile(file_name)

print(ipfshash)
