from web3 import Web3, HTTPProvider, IPCProvider
import web3
import ipfsapi
# import hashlib
import sys
import json
class WWeb3():

    def __init__(self, abiFile, dataStoreContractAddress):
        with open('contract_abi.json', 'r') as abi_definition:
            self.abi = json.load(abi_definition)
        self.dataStoreContractAddress = dataStoreContractAddress
        self.w3 = Web3(IPCProvider('/home/a/.ethereum/geth.ipc'))
        self.account = '0xe92Ed6Ef45E14C186182Bf32B46Ce53B5F0C55F0'
        self.w3_contract = self.w3.eth.contract(self.abi, self.dataStoreContractAddress) 

    def addChecksum(self, recorder, ipfshash):
        self.w3.personal.unlockAccount(str(self.account), 'password123')
        self.w3_contract.transact({'from': self.account}).recordIpfsEntry(ipfshash, str(recorder))


class Ipfs():

    def __init__(self, ip, port):
        self.ipfs_api = ipfsapi.connect(ip, port)
        self.ipfsHashList = []

    
    def uploadFile(self, objectID):
        # objectID is the name of the file to upload
        response = self.ipfs_api.add(objectID)
        for key in response.keys():
            if key == 'Hash':
                ipfsHash = response[key]
            elif key == 'Name':
                fileName = response[key]
        if ipfsHash in self.ipfsHashList:
            print('[WARN] - IPFS Hash Already In List, reporting to notice board about overwrite attempt')
            return false
        else:
            self.ipfsHashList.append(ipfsHash)
            return ipfsHash
    
    def __updateList(self, ipfsHash):
        self.ipfsHashList.append(ipfsHash)
        with open('ipfshash.txt', 'a') as fh:
            fh.write('%s\n' % self.ipfsHashList)
        


