#! /bin/bash

# currently broken
r
nodeone="/ip4/25.196.147.100/tcp/4001/ipfs/QmaMqSwWShsPg2RbredZtoneFjXhim7AQkqbLxib45Lx4S"
sharedkey="700370fcecd615e83bfde84708b8b812c144188f5f02b475a1fd8e90c4f5fa5f"

echo "$sharedkey" > ~/.ipfs/swarm.key
# This file is used to bootstrap the IPFS daemon node and join the swarm

# kill bootstrap list
ipfs bootstrap rm --all

# Add bootstrap node (This is a trusted node, ran by yours truly)
ipfs bootstrap add "$nodeone"
# Force IPFS to only connect to private networks, IPFS will shutdown otherwise
export LIBP2P_FORCE_PNET=1
# Start IPFS Daemon in background
ipfs daemon &