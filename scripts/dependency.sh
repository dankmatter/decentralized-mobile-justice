#! /bin/bash



echo "[INFO] Installing pip3"
sudo apt-get install python3-pip -y >> /dev/null

echo "[INFO] Installing web3py Dependencies"
sudo apt-get install libssl-dev -y

echo "[INFO] Installing git"
sudo apt-get install git -y

mkdir /tmp/install
cd /tmp/install
git clone https://github.com/pipermerriam/web3.py.git
cd web3.py
pip install -r requirements-dev.txt
sudo python3.5 setup.py install
cd /tmp
rm -rf install
echo "[INFO] Installing IPFS API for Python 3"
pip3 install ipfsapi

wget https://ipfs.io/ipns/dist.ipfs.io/go-ipfs/v0.4.10/go-ipfs_v0.4.10_linux-amd64.tar.gz

tar zxvf go-ipfs_v0.4.10_linux-amd64.tar.gz

cd go-ipfs*

sudo ./install.sh