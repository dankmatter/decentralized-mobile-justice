package main

import (
	"fmt"
	"time"
)

func main() {
	hourOfDay := time.Now().Hour()
	greeting := getGreeting(hourOfDay)
	fmt.Println(greeting)
}

func getGreeting(_hourOfDay int) string {
	if _hourOfDay >= 17 {
		return "Good Evening"
	} else if _hourOfDay > 12 {
		return "Good Afternoon"
	} else if _hourOfDay == 12 {
		return "Good Midday"
	}
	return "Good Morning"
}
