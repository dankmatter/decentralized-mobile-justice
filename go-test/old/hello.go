package main

import (
	"fmt"
	"os"
)

func main() {
	// dynamically determine variable type
	args := os.Args
	var message string = "Hello I am gopher"
	// conditional doesn't need braces
	if len(args) > 1 {
		message = args[1]
		fmt.Println(message)
	} else {
		fmt.Println(message)
	}
}
