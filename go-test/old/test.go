package main

import (
	"fmt"
)

// <name>(<var-name> <var-type>) <return-type> {
//}
// if using multiple variables of the same time:
/// func add(x,y int) int {}
func add(x int, y int) int {
	return x + y
}

// (string, string) indicates two string return values
func swap(a, b string) (x, y string) {
	x = b
	y = a
	return
}
f//unc main() {
	a, b := swap("hello", "world")
	fmt.Println(a, b)
}
