pragma solidity 0.4.11;


contract BytesToString {
    function bytes32ToString(bytes32 x) public constant returns (string) {
    bytes memory bytesString = new bytes(32);
    uint charCount = 0;
    for (uint j = 0; j < 32; j++) {
        byte char = byte(bytes32(uint(x) * 2 ** (8 * j)));
        if (char != 0) {
            bytesString[charCount] = char;
            charCount++;
        }
    }
    bytes memory bytesStringTrimmed = new bytes(charCount);
    for (j = 0; j < charCount; j++) {
        bytesStringTrimmed[j] = bytesString[j];
    }
    return string(bytesStringTrimmed);
   }
}

contract Owner {
    
    address public owner;

    function Owner() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address _newOwner) onlyOwner {
        owner = _newOwner;
    }
}

contract SafeMath {
      function mul(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }

  function div(uint256 a, uint256 b) internal constant returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }

  function sub(uint256 a, uint256 b) internal constant returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  function add(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}


contract DataReceive is Owner, SafeMath, BytesToString {

    
    address public tokenContractAddress;

   // testing struct to track user entries
    struct Uploaders {
        address person;
        uint256 numContributions;
        // unused for now, will be used to track credits;
        uint256 credits;
        mapping (bytes32 => string) entryTracker;
    }
    
    mapping (address => mapping (uint256 => string)) public entryTracker;

    // Used to keep trtack of  each uploader, how many videos they uploded, how many credits, etc...
    mapping (address => Uploaders) public uploaderTracker;
    
    // used to check if a user has uploaded before
    mapping (address => bool) public hasUploaded;
  
    // tracks credits of user 
    mapping (address => uint256) public creditBalances;

    // mapping used to track checksum to ipfshash
    mapping (bytes32 => string)  ipfsChecksumToHash;

    // tracks to see if a ipfsHash has been entered;
    mapping (bytes32 => bool) public ipfsHashEntered;

    // Event to log data entry
    event DataEntry(address indexed _recorder, string indexed _ipfsHash, bytes32 indexed _ipfsChecksum);
  
    // Logs a token transfer
    event CreditTransfer(address indexed _this, address indexed _to, uint256 _amount);
    // this function can only be called by the contract when a user hasn't uploaded before

    function registerRecorder(address _recorder) private returns (bool success) {
        Uploaders storage t = uploaderTracker[_recorder];
        t.person = _recorder;
        t.numContributions = 0;
        t.credits = 0;
        uploaderTracker[_recorder] = t;
        return true;
    }
    function addEntry(address _recorder, string _ipfsHash) external {
        if(!hasUploaded[_recorder]) {
            if(!registerRecorder(_recorder)) {
                // if registering the recorder fails, revert all state changes
                revert();
            }
        } 
        bytes32 _ipfsChecksum = sha256(_ipfsHash);
        // checks to see that this particular ipfs hash hasn't been uploaded before
        require(!ipfsHashEntered[_ipfsChecksum]);  
        // makes sure the person calling the function isn't the recorder
        require(_recorder != msg.sender);
        uploaderTracker[_recorder].numContributions += 1; 
        uploaderTracker[_recorder].credits += 1; 
        uploaderTracker[_recorder].entryTracker[_ipfsChecksum] = _ipfsHash;
        if(!hasUploaded[_recorder]) {
            hasUploaded[_recorder] = true;
        }
        ipfsHashEntered[_ipfsChecksum] = true;
        // Notifies blockchain that data was sotred
        DataEntry(_recorder, _ipfsHash, _ipfsChecksum);
    }

    function isValidEntry(string _ipfsHash) public constant returns (bool yes) {
        bytes32 _ipfsChecksum = sha256(_ipfsHash);
        if(!ipfsHashEntered[_ipfsChecksum]) {
            return false;
        }
        return true;
    }

   function getChecksum(string _ipfsHash) public constant returns (bytes32 checksum) {
       bytes32 _ipfsChecksum = sha256(_ipfsHash);
       return _ipfsChecksum;
   }
    function verifiyDataIntegrity() public constant returns (bool success) {
        return true;
    }

    function() payable {
        assert(msg.value == 0);
    }
 }