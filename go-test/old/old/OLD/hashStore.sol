pragma solidity ^0.4.11;

contract Owner {
    
    address public owner;

    function Owner() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address _newOwner) onlyOwner {
        owner = _newOwner;
    }
}

contract SafeMath {
      function mul(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }

  function div(uint256 a, uint256 b) internal constant returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }

  function sub(uint256 a, uint256 b) internal constant returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  function add(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}

contract hashStore is SafeMath, Owner {

    // will be unique per owner
    struct ipfsHashTracker {
        uint8 id;
        string ipfsHash;
        bytes32 ipfsChecksum;
    }

    struct submissionTracker {
        address uploader;
        mapping (uint8 => ipfsHashTracker) entries;
    }

    mapping (address => submissionTracker) public users;
    mapping (address => bool) public userAdded;

    function addNewUser(address _user) private returns (bool success) {
        // initializes a new user
        users[_user] = submissionTracker(_user);
        userAdded[_user] = true;
    }

    function uploadHash() public returns (bool success) {
        if(!userAdded[_uploader]) {
            // user is not previously in our database
        }
    }

}