pragma solidity 0.4.13;

contract Campaign {
    
    address public owner;
    uint    public deadline;
    uint    public goal;
    uint    public fundsRaised;

    struct FunderStruct {
        address funder;
        uint    contribution;
    }

    FunderStruct[] public funderStructs;


    function Campaign(uint duration, uint _goal) {
        owner = msg.sender;
        deadline = block.number + duration;
        goal = _goal;
    }

    function contribute() public payable returns (bool success) {
        if (msg.value == 0) 
            revert;
        fundsRaised += msg.value;
        FunderStruct memory newFunder;
        newFunder.funder = msg.sender;
        newFunder.contribution = msg.value;
        funderStructs.push(newFunder);
        return true;
    }

    function withdrawFunds() public returns (bool success) {
        return true;
    }

    function sendRefunds() public returns (bool success) {
        return true;
    }
}