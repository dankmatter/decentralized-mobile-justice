pragma solidity 0.4.13;

contract Owner {
    
    address public owner;

    function Owner() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address _newOwner) onlyOwner {
        owner = _newOwner;
    }
}

contract SafeMath {
      function mul(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }

  function div(uint256 a, uint256 b) internal constant returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }

  function sub(uint256 a, uint256 b) internal constant returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  function add(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}

contract IpfsTracker is SafeMath, Owner {
    
    address     public tokenContractAddress;
    uint256     public totalNumberOfUploads;
    uint256     public totalNumberOfUsers;
    uint256     public creditsPerUpload;
    uint256     public firstTimeUploadBonus;
    bool        public uploadsEnabled;

    struct UploaderTracker {
        address owner;
        uint256 numberOfUploads;
        uint256 uploadCredits;
        mapping (string => bytes32) ipfsHashTracker;
    }

    /// @notice array of uploaders used to contain the structs identifying each uploaders statistics
    UploaderTracker[] public uploadTrackerArray;

    // Used to keep track of the uploadTrackerArray element ID for each uploader
    mapping (address => uint256) public uploaderArrayID;
    // track balances used for token rewards
    mapping (address => uint256) public balances;
    // Used to track whether or not a user has already been registered
    mapping (address => bool)   public uploaderRegistered;
    // Used to track uploader ID and their associated UploaderTracker struct
    mapping (uint256 => UploaderTracker) public uploadTracker;
    // serves as a global list of all IPFS Entries uploaded
    mapping (string => bytes32) globalIpfsHashTracker;

    event NewUserRegistered(address indexed _uploader, bool indexed userRegistered);

    modifier requireUploadsEnabled() {
        require(uploadsEnabled);
        _;
    }

    /// @notice constructor
    function IpfsTracker(address _tokenContractAddress) {
        tokenContractAddress = _tokenContractAddress;
        totalNumberOfUploads = 0;
        totalNumberOfUsers = 0;
    }

    
    /// @notice high-level retrieval function for IPFS Checksum
    function retrieveIpfsHashChecksum(string _ipfsHash) 
        constant 
        public 
        returns (bytes32 checksum) 
    {
        return globalIpfsHashTracker[_ipfsHash];
    }


    /// @notice low level user register function is called by recordIpfsEntry
    /// @param _uploaderAddress specifies the address of the uploader
    function registerUser(address _uploaderAddress) 
        private 
        returns (bool success) 
    {
            // initialize struct in memory
            UploaderTracker memory u;
            // set uploader
            u.owner = _uploaderAddress;
            // initialize with 0 uploads
            u.numberOfUploads = 0;
            // initialize with 0 credits
            u.uploadCredits = 0;
            // push the array to storage state object
            uploadTrackerArray.push(u);
            // sets the element ID to the total number of users (0 to start)
            uploaderArrayID[_uploaderAddress] = totalNumberOfUsers;
            // Logs the user as registered
            uploaderRegistered[_uploaderAddress] = true;
            // Updates total number of users
            totalNumberOfUsers += add(totalNumberOfUsers, 1);
            // Notifies blockchain that a new user was registered
            NewUserRegistered(_uploaderAddress, true);
            return true;
    }

    /// @notice upload function
    function recordIpfsEntry(string _ipfsHash, address _uploaderAddress) 
        public 
        requireUploadsEnabled 
        returns (bool success) 
    {
        bytes32 _checksum = sha256(_ipfsHash);
        if (!uploaderRegistered[_uploaderAddress]) {
            if (!registerUser(_uploaderAddress)) {
                revert();
            }
        }
        UploaderTracker storage u = uploadTrackerArray[uploaderArrayID[_uploaderAddress]];
        u.ipfsHashTracker[_ipfsHash] = _checksum;
    }

}