pragma solidity 0.4.13;

contract Owner {
    
    address public owner;

    function Owner() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address _newOwner) onlyOwner {
        owner = _newOwner;
    }
}

contract SafeMath {
      function mul(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }

  function div(uint256 a, uint256 b) internal constant returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }

  function sub(uint256 a, uint256 b) internal constant returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  function add(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}


// Current Bugs
// Credits aren't given properly, no easy way to retrieve the uploads by a user,
contract IpfsTracker is SafeMath, Owner {
    
    address     public tokenContractAddress;
    uint256     public totalNumberOfUploads;
    uint256     public totalNumberOfUsers;
    uint256     public creditsPerUpload = 1;
    uint256     public firstTimeUploadBonus = 20;
    uint256     public bonusPeriod;
    bool        public uploadsEnabled;

    struct UploaderTracker {
        address owner;
        uint256 numberOfUploads;
        uint256 uploadCredits;
        // tracks checksum sha256 of ipfsHash
        mapping (uint256 => mapping (string => bytes32)) ipfsChecksumTracker;
        // tracks plaintext ipfs hashes
        mapping (uint256 => mapping (bytes32 => string)) ipfsHashTracker;
    }


    // Used to keep track of the associated IPFS Checksum for upload number of given user
    mapping (address => mapping (uint256 => bytes32)) uploaderChecksumTracker;
    // Used to keep track of the associated IPFS hash for upload number of given user
    mapping (address => mapping (uint256 => string)) uploaderHashTracker;
    // Used to keep track of the uploadTrackerArray element ID for each uploader
    mapping (address => uint256) public uploaderIdTracker;
    // track balances used for token rewards
    mapping (address => uint256) public balances;
    // Used to track whether or not a user has already been registered
    mapping (address => bool)   public uploaderRegistered;
    // Used to track uploader ID and their associated UploaderTracker struct
    mapping (uint256 => UploaderTracker) public uploadTracker;
    // Used to keep track of whether or not a hash has been uploaded
    mapping (string => bool) ipfsHashUploaded;
    // serves as a global list of all IPFS Entries uploaded
    mapping (string => bytes32) globalIpfsChecksumTracker;
    // Tracks the IPFS Hash in plaintext
    mapping (bytes32 => string) globalIpfsStringTracker;

    event NewUserRegistered(address indexed _uploader, bool indexed userRegistered);
    event NewFileUploaded(address indexed _uploader, string indexed _name, bytes32 indexed _checksum, bool Uploaded);
    event UploadsEnabled(bool indexed Enabled);

    modifier requireUploadsEnabled() {
        require(uploadsEnabled);
        _;
    }

    /// @notice constructor
    function IpfsTracker(address _tokenContractAddress) {
        tokenContractAddress = _tokenContractAddress;
        totalNumberOfUploads = 0;
        totalNumberOfUsers = 0;
        uploadsEnabled = false;
    }

    /// @notice Allows you to look up the IPFS Checksum of upload #
    /// @param _uploadNumber is the upload # of a user you wish to lookup a checksum for
    function getUploaderChecksum(address _uploaderAddress, uint256 _uploadNumber)
        constant
        public
        returns (bytes32 checksum)
    {
        return uploaderChecksumTracker[_uploaderAddress][_uploadNumber];
    }

    /// @notice Allows you to look up the IPFS Hash of upload # 
    /// @param _uploadNumber is the upload # of a user you wish to lookup a hash for
    function getUploaderHash(address _uploaderAddress, uint256 _uploadNumber)
        constant
        public
        returns (string ipfsHash)
    {
        return uploaderHashTracker[_uploaderAddress][_uploadNumber];
    }

    /// @notice admin only, enables uploads
    function enableUploads()
        public
        onlyOwner
        returns (bool enabled)
    {
        uploadsEnabled = true;
        UploadsEnabled(true);
        return true;
    }

    /// @notice high-level ipfs hash uploaded checker
    function ipfsHashUploadStatus(string _ipfsHash)
        constant
        public
        returns (bool uploaded)
    {
        return ipfsHashUploaded[_ipfsHash];
    }

    /// @notice high-level retrieval function for IPFS Checksum
    function retrieveIpfsHashChecksum(string _ipfsHash) 
        constant 
        public 
        returns (bytes32 checksum) 
    {
        return globalIpfsChecksumTracker[_ipfsHash];
    }

    /// @notice high-level retrieval function for IPFS Hashes in Plaintext
    function retrieveIpfsPlaintext(bytes32 _ipfsChecksum) 
        constant
        public
        returns (string plaintext)
    {
        return globalIpfsStringTracker[_ipfsChecksum];
    }


    /// @notice low level user register function is called by recordIpfsEntry
    /// @param _uploaderAddress specifies the address of the uploader
    function registerUser(address _uploaderAddress) 
        private
        returns (bool success)
    {
            // Gets ID for new user
            uint256 _uploaderID = add(totalNumberOfUsers, 1);
            // initializes the Struct for our new user
            uploadTracker[_uploaderID] = UploaderTracker(_uploaderAddress, 0, 0);
            // Registers user as true
            uploaderRegistered[_uploaderAddress] = true;
            // Updates total number of users registered with the system
            totalNumberOfUsers = _uploaderID;
            if (now <= bonusPeriod) {
                // Initializes account with bonus balance
                balances[_uploaderAddress] = firstTimeUploadBonus;
            } else {
                // `now` is past 
                balances[_uploaderAddress] = 0;
            }
            // Sets uploader ID for easy lookup
            uploaderIdTracker[_uploaderAddress] = _uploaderID;
            // Notifies blockchain that a new user was registered
            NewUserRegistered(_uploaderAddress, true);
            return true;
    }

    /// @notice upload function
    function recordIpfsEntry(string _ipfsHash, address _uploaderAddress) 
        public 
        requireUploadsEnabled 
        returns (bool success) 
    {
        if (ipfsHashUploaded[_ipfsHash]) {
            revert();
        }
        bytes32 _checksum = sha256(_ipfsHash);
        if (!uploaderRegistered[_uploaderAddress]) {
            registerUser(_uploaderAddress);
        }
        uint256 _uploaderID = uploaderIdTracker[_uploaderAddress];
        UploaderTracker storage u = uploadTracker[_uploaderID];
        u.numberOfUploads = add(u.numberOfUploads, 1);
        u.ipfsHashTracker[u.numberOfUploads][_checksum] = _ipfsHash;
        u.ipfsChecksumTracker[u.numberOfUploads][_ipfsHash] = _checksum;
        uploaderChecksumTracker[_uploaderAddress][u.numberOfUploads] = _checksum;
        uploaderHashTracker[_uploaderAddress][u.numberOfUploads] = _ipfsHash;
        u.uploadCredits = add(u.uploadCredits, creditsPerUpload);
        balances[_uploaderAddress] = add(balances[_uploaderAddress], creditsPerUpload);
        globalIpfsChecksumTracker[_ipfsHash] = _checksum;
        globalIpfsStringTracker[_checksum] = _ipfsHash;
        ipfsHashUploaded[_ipfsHash] = true;
        NewFileUploaded(_uploaderAddress, _ipfsHash, _checksum, true);
        return true;
    }

}