pragma solidity 0.4.13;

contract Owned {
    
    address public owner;

    function Owned() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address _newOwner) onlyOwner {
        owner = _newOwner;
    }
}

contract SafeMath {
  
    function mul(uint256 a, uint256 b) internal constant returns (uint256) {
        uint256 c = a * b;
        assert(a == 0 || c / a == b);
        return c;
    }

    function div(uint256 a, uint256 b) internal constant returns (uint256) {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
    }

    function sub(uint256 a, uint256 b) internal constant returns (uint256) {
        assert(b <= a);
        return a - b;
    }

    function add(uint256 a, uint256 b) internal constant returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}

contract DMJToken is SafeMath, Owned {

    uint256     public totalSupply;
    uint8       public decimals;
    string      public symbol;
    string      public name;
    bool        public transfersFrozen;

    mapping (address => uint256) public balances;
    mapping (address => mapping (address => uint256)) public allowance;

    event Transfer(address indexed _from, address indexed _to, uint256 _amount);
    event Approve(address indexed _owner, address indexed _spender, uint256 _allowance);

    function DMJToken(string _name, string _symbol, uint8 _decimals, uint256 _totalSupply) {
        totalSupply = _totalSupply;
        decimals = _decimals;
        symbol = _symbol;
        name = _name;
    }

    function transfer(address _to, uint256 _amount) public returns (bool success) {
        require(!transfersFrozen);
        require(balances[msg.sender] >= _amount);
        require(balances[_to] + _amount > balances[_to]);
        balances[msg.sender] = sub(balances[msg.sender], _amount);
        balances[_to] = add(balances[_to], _amount);
        Transfer(msg.sender, _to, _amount);
        return true;
    }

    function transferFrom(address _owner, address _to, uint256 _amount) public returns (bool success) {
        require(!transfersFrozen);
        require(allowance[_owner][msg.sender] >= _amount);
        return true;
    }

}