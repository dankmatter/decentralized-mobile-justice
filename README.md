# Decentralized Mobile Justice 

Several years ago there was a project done along with ACLU (around the time of the Eric Gardner police brutality case) to build a system where users could stream footage a police incident, and store it locally on their phone, but also stream it to remote servers in case the phone was compromised and video evidence deleted. This was a great project but it suffered from one problem: centralization. For whatever reason it appears the project is now defunct, so in essence this is a reboot of that project, but instead of storing the data on central servers, it will be stores usaing a distributed, and decentralized filesystem protocol called IPFS (this may change in the future) to store the data. This data will then be checksumed, and the checksum will be uploaded to the ethereum blockchain in an immutable storage container of a smart contract this way when viewing the video you can run a checksum, and compare the hash with a known good, and non-changeable hash which will help verify video authenticity (this verification system will likely change and become more advanced as time goes on)

It will feature a token based incentive system for contributing to the network, whether it be recording incidences, or running one of the servers that receives a video stream. 

# ERC20 Token and Incentive For Using The DMJ Network

In order to reward users and provided an incentive to use the network. An ERC20 or ERC23 token will be generated to serve this purpose. Rewards will be dstributed to people recording videos, people who curate videos, people running the livepeer nodes to receive video, and people running the storage nodes to provide secure, redundant data storage and  integrity verification.

Token distribution will be done through Air drops, and community events as the project matures.

There will be no ICO or any crowdfunding campaign. All costs are being paid by myself (Postables), however I do accept donations, 50% of which get donated to charity organizations. I will be taking this project to completion regardless of whether or not any funds are raised in any capacity, even if I'll be the only developer. I personally think this is something that could really benefit a lot of people so I'm willing to do whatever it takes (even though at this point it looks like I'll have to learn 3-4 new languages).

# Communication Channels

There is a community discord channel for the DMJ project, simply click the following permanent invite link:    
* https://discord.gg/aE3eV4

# Looking to contribute?

Currently there are two developers working on this project, but I would love for more to join.

Skill sets needed:
* Marketing
* PR
* Web design
* NodeJS Developer
* Mobile Application Developer
* Go developer
* Project Advisors
* IPFS wizards
* Storage wizards
* Front end Developers
* UI Designers

# Project Road Map

As of this moment, there is no set roadmap with specific dates. However this is an open-source, and volunteer based project where anyone is allowed to contribute. In the future I would like to build in a form of geo-location into a mobile application to alert nearby users of the application to cases of human rights abuses.

The project is still very much in it's early phases and the overall scope, and architecture of the project is likely to evolve overtime.

# Donations

If you wish to donate, you can do so anonymously, or let me know your name and you'll be listed on this README.md as a contributor.

Donations are split 50/50 with 25% being donated to the ACLU, 25% being donated to Amnesty International, and the remaining 45% being split amongst team members, and 5% being used to maintain services needed for this project to run (paid for JIRA, confluence, etc...).

##Should you wish to donate send ETH or any ERC20 tokens to the following address:
##ETH: 0x00e2EFA637A08fA51571D41E07DeEfF99B7C806c

##BTC: 1E8F4xDqMNLGXu7UfYA4MAMxqst5mTXsf5
##BCH: 1MQUN5a8yrumapw62mFB468m9CtpEipkWc
##LTC: LR6VJFnxZYhN4eekUghz3YN1bbp6NBKPVb
##NEO/GAS: Ac13JiLc4efGyhfkAZxTZ2THvWPoXjCyrS
##FIAT: Please contact me  directly

# Contributors/Backers/Donators List
